# Written by Lukas Gemein
import pandas as pd
import numpy as np

#TODO: implement more features

def mean(df):
    return df.mean()

def energy(df):
    return (df * df).mean()

def power(df):
    return (df * df).sum()

def var(df):
    return df.var()

def entropy(df):
    squared = (df * df)
    return -((squared * np.log2(squared)).sum())

def boundedvariation(df):
    return df.diff().abs().sum().divide(df.max() - df.min())

# def relativescaleenergy(df):
#     return