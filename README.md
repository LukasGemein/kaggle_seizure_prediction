This code was written to do epileptic seizure prediction on datasets hosted on https://www.kaggle.com/c/melbourne-university-seizure-prediction

1. Download
2. Run python preprocess.py <parent directory of datasets> <directory to store features> [nWindows]
3. Run python predict.py <parent of features> <submission directory> <nWindows>

Requires numpy, pandas, h5py, PyWavelets, sklearn, dill, tblib, inspect, gc, ...