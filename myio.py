# Written by Lukas Gemein
from sklearn import preprocessing
from scipy import io as sio
import numpy as np
import cPickle
import datetime
import h5py
import glob
import csv
import re
import os

#______________________________________________________________________________________________________________________
# natural key definition taken from deep (kaggle forum)
def natural_key(string_):
    return [int(s) if s.isdigit() else s for s in re.split(r'(\d+)', string_)]

#______________________________________________________________________________________________________________________
# list all files of patient and case within the given source directory.
def getFiles(sourcedir, patient_id, case):
    if case == "interictal": c, s, new = "train", '0', ''
    elif case == "preictal": c, s, new = "train", '1', ''
    elif case == "test": c, s, new = "test", '', '_new'

    return sorted(glob.glob(sourcedir + c + '_' + str(patient_id) + new + "/*" + s + ".mat"), key=natural_key)

#______________________________________________________________________________________________________________________
# inefficient, not needed on farmrechner
# check if training files are labeled as safe by kaggle admin
def safetyCheck(indir, safetycsv, patient_id, files):
    # read from csv file all the safe files
    path = os.path.join(indir, safetycsv)
    safe = {}
    with open(path, 'r') as csvfile:
        reader = csv.reader(csvfile, delimiter=',')
        # skip header of csv
        reader.next()
        # read information and store
        for [file, filecase, issafe] in reader:
            file, issafe = file, False if issafe == '0' else True
            safe[file] = issafe

    # find unsafe files in filelist
    unsafeids = []
    for index, file in enumerate(files):
        matfile = file.split('/')[-1]
        if not safe[matfile]: unsafeids += [index]

    # delete those from list
    for fileid in unsafeids[::-1]:
        del files[fileid]
 
    return files

#______________________________________________________________________________________________________________________
# read filename matlab file from directory indir
def readMat(indir, filename):
    filepath = os.path.join(indir, filename)
    return sio.loadmat(filepath, squeeze_me=True, struct_as_record=False)

#______________________________________________________________________________________________________________________
# write matlab file to outdir
def writeMat(outdir, filename, data):
    path = os.path.join(outdir, filename.split('/')[-1])
    sio.savemat(path, data, do_compression=True)
    print "wrote mat to ", path

#______________________________________________________________________________________________________________________
# write skipped files to file to read back later
def writeSkipped(outdir, file):
    path = os.path.join(outdir, "skippedtestfiles.txt")
    f = open(path, 'a')
    f.write(file + '\n')
    f.close()
    print "wrote skipped", file
#______________________________________________________________________________________________________________________
# read skipped files from file 
def readSkipped(indir):
    files = None
    path = os.path.join(indir, "skippedtestfiles.txt")
    if os.path.exists(path):
        f = open(path, 'r')
        files = f.readlines()
        f.close()
    return files

#______________________________________________________________________________________________________________________
def getH5file(indir, patient_id):
    return h5py.File(os.path.join(indir, 'patient_' + str(patient_id) + '.h5'), 'r')

#______________________________________________________________________________________________________________________
# create hdf file structure levels: one file per patient with 
# interictal/preictal/test, time/frequency, mean/max/min/... bzw. spectrum/entropy..., segment 1,2,3,4... with ch x windows
def createHDF5Structure(outdir, patient_id, cases, domains, features_time, features_frequency):
    hdf5filepath = os.path.join(outdir, "patient_" + str(patient_id) + ".h5")
    # create new hdf5 file for writing, creates new file if not yet existing
    if os.path.exists(hdf5filepath):
        # allow to compute files incrementally and add them to existing hdf5file works. overwriting entries does NOT work!
        print "file", hdf5filepath, "already exists. you better know what you are doing, otherwise the proram will crash"
        print "you can run the program like this if you want to add not yet eisting entries to the existing groups in the hdf5 file"
        print "if you don't want this, delete the hdf5 file and restart to prevent a crash\n\n"
        return hdf5filepath

    else:
        print "Creating new hdf5file", hdf5filepath, '\n\n'
        hdf5file = h5py.File(hdf5filepath, 'w')
        for case in cases:
            hdf5file.create_group(case)
            for domain in domains:
                if domain == "frequency" and len(features_frequency) > 0:
                    hdf5file[case].create_group(domain)
                    for feature_frequency in features_frequency:
                        hdf5file[case][domain].create_group(feature_frequency)
                
                elif domain == "time" and len(features_time) > 0:
                    hdf5file[case].create_group(domain)
                    for feature_time in features_time:
                        hdf5file[case][domain].create_group(feature_time)

        hdf5file.close()
    return hdf5filepath

#______________________________________________________________________________________________________________________
def writeToHDF5(features, hdf5filepath, filelist, case, features_frequency, features_time, normalize):
    # open hdf5file with existing structure to update content
    hdf5file = h5py.File(hdf5filepath, 'r+')
    if normalize:
        min_max_scaler = preprocessing.MinMaxScaler()
    for fileid, filename in enumerate(filelist):
        # features returned from process manager has 2 entries for every file, that is time and frequency features
        freqresults = features[2 * fileid]
        timeresults = features[2 * fileid + 1]

        # freqresults is none if no features were computed
        if freqresults is not None:
            for featureid, frequencyfeature in enumerate(features_frequency):
                # store "raw" feature to file
                if not normalize:
                    hdf5file[case]["frequency"][frequencyfeature][filename.split('/')[-1].split('.')[0]] = freqresults[featureid]
                # also stored normalized features
                else:
                    # usually, normalization should not be done here. but due to the structure of the code this seems to be the best place
                    scaled = min_max_scaler.fit_transform(freqresults[featureid])
                    hdf5file[case]["frequency"][frequencyfeature][filename.split('/')[-1].split('.')[0]] = scaled

        # timeresults is none if no features were computed
        if timeresults is not None:
            for featureid, timefeature in enumerate(features_time):
                # store "raw" feature to file
                if not normalize:
                    # usually, normalization should not be done here. but due to the structure of the code this seems to be the best place
                    hdf5file[case]["time"][timefeature][filename.split('/')[-1].split('.')[0]] = timeresults[featureid]
                # also stored normalized features
                else:
                    scaled = min_max_scaler.fit_transform(timeresults[featureid])
                    hdf5file[case]["time"][timefeature][filename.split('/')[-1].split('.')[0]] = scaled

    hdf5file.close()

#______________________________________________________________________________________________________________________
def traverseHDF5(file, names, counts, features_frequency, features_time, level):
    if not isinstance(file, h5py.File) and not isinstance(file, h5py.Dataset) and isinstance(file, h5py.Group):
        groupname = file.name.split('/')[-1]
        # print level, groupname, groupname not in ["frequency", "time"]
        if level == 1 and groupname not in ["interictal", "preictal", "test"]: return
        elif level == 2 and groupname not in ["frequency", "time"]: return
        elif level == 3 and "frequency" in file.name and groupname not in features_frequency: return
        elif level == 3 and "time" in file.name and groupname not in features_time: return

    if isinstance(file, h5py.Dataset):
        # print "adding", file.name
        name = file.name
        names += [name]
        counts[name.split('/')[1]] += 1
 
    if isinstance(file, h5py.File) or isinstance(file, h5py.Group):
        for key, val in dict(file).iteritems():
            traverseHDF5(val, names, counts, features_frequency, features_time, level+1)

#______________________________________________________________________________________________________________________
def restoreHDF5Structure(file, features_frequency, features_time):
    names, counts = [], {'interictal': 0, 'preictal': 0, 'test': 0}
    traverseHDF5(file, names, counts, features_frequency, features_time, 0)
    # -> interictal, preictal, test / frequency, time / ...
    return (sorted(names, key=natural_key), counts)

#______________________________________________________________________________________________________________________
def readFromHDF5(h5file, names, normalize):
    hdf5Features = []
    for index, name in enumerate(names):
        # print index, name, np.array(file[name]).shape
        h5Entry = np.array(h5file[name])
        # usually, normalization should not be done here. but due to the structure of the code this seems to be the best place
        if normalize:
            min_max_scaler = preprocessing.MinMaxScaler()
            h5Entry = h5Entry.reshape(-1, 1)
            h5Entry = min_max_scaler.fit_transform(h5Entry)
        # hdf5Features = h5Entry if len(hdf5Features) == 0 else np.hstack((hdf5Features, h5Entry))
        hdf5Features += [h5Entry]
    h5file.close()
    return hdf5Features

#______________________________________________________________________________________________________________________
def writeModel(model, outdir, filename):
    now = datetime.datetime.now()
    path = os.path.join(outdir, str(now.strftime("%Y-%m-%d-%H-%M")) + '_' + filename)
    picklefile = open(path, 'w')
    cPickle.dump(model, picklefile) 
    picklefile.close()

#______________________________________________________________________________________________________________________
# create kaggle submission
def createSubmission(indir, outdir, testnames, score, predictions):
    now = datetime.datetime.now()
    sub_filename = 'submission_' + str(score) + '_' + str(now.strftime("%Y-%m-%d-%H-%M")) + '.csv'
    sub_file = open(os.path.join(outdir, sub_filename), 'w')
    sub_file.write('File,Class\n')
    # print len(testnames), len(predictions)
 
    for index, testname in enumerate(testnames):
        sub_file.write(testname + ',' + str(predictions[index]) + '\n')

    skippedFiles = readSkipped(indir)
    if skippedFiles is not None:
        for skippedFile in skippedFiles:
            sub_file.write(skippedFile.strip().split('/')[-1] + ",0.5\n")
    sub_file.close()