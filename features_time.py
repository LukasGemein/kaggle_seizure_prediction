# Written by Manuel Blum
# Adapted by Lukas Gemein to do calculations on non-overlapping windows / whole data frames only!
import pandas as pd
import numpy as np

# TODO: implement more features

# Imagine that you have zero cookies, and you split them evenly among 
# zero friends. How many cookies does each person get? See? It doesn't 
# make sense. And Cookie Monster is sad that there are no cookies, and 
# you are sad that you have no friends.
def div0(x):
    x[x<1e-9] = 1e-9
    return x

####### time domain

# Time Mean
def mean(df):
    return df.mean()

# Time Median
def median(df):
    return df.median()

# Time Variance
def var(df):
    return df.var()

# Time Standard Deviation
def std(df):
    return df.std()

# Time Line Length
def linelength(df):
    return df.diff().abs().sum()

# Time Minimum
def min(df):
    return df.min()

# Time Maximum
def max(df):
    return df.max()

# Time Skewness
def skew(df):
    return df.skew()

# Time Kurtosis
def kurt(df):
    return df.kurt()

# Time Energy
def energy(df):
    return (df * df).mean()

# Time Complexity
def complexity(df):
    diff1 = df.diff()
    diff2 = diff1.diff()
    sigma = df.std()
    sigma1 = diff1.std()
    sigma2 = diff2.std()
    return (sigma2 / div0(sigma1)) / div0((sigma1 / div0(sigma)))

# Time Mobility
def mobility(df):
    diff = df.diff()
    return diff.std() / div0(df.std())

# Time Nonlinear Energy
def nonlinenergy(df):
    squares = np.square(df[1:-1]) - df[2:].values * df[:-2].values
    return squares.mean()
    
# Time Fractal Dimension
def fractaldim(df):
    diff = df.diff()
    sum_of_distances = np.sqrt(diff * diff).sum()
    max_dist = df.apply(lambda df: np.max(np.sqrt(np.square(df - df[0]))))
    return np.log10(sum_of_distances) / div0(np.log10(max_dist))

# Time Zero Crossing
def zerocrossing(df):
    epsilon = 0.01
    norm = df - df.mean()
    return ((norm[:-5] < epsilon) & (norm[5:].values > epsilon)).sum()

# Time Zero Crossing Derivative
def zerocrossingdev(df):
    epsilon = 0.01
    diff = df.diff()
    norm = diff - diff.mean()
    return ((norm[:-5] < epsilon) & (norm[5:].values > epsilon)).sum()