# written by Lukas Gemein
import matplotlib
matplotlib.use('Agg')

from sklearn.ensemble import RandomForestClassifier, AdaBoostClassifier
from sklearn import cross_validation
from sklearn import preprocessing
import matplotlib.pyplot as plt
from sklearn import metrics
import numpy as np
import sys
import os

# custom modules
import myio

# TODO: combine features of different time window sizes!?!
# hacky oversampling is added
# TODO: fix cross validation scores, respect temporal structure of data when doing cross validation
# TODO: fix std?
# TODO: log importances and scores to file?
# TODO: move plotting?

# TODO: do some kind of search over parameters? e.g. use feature importances to adapt the features that are
# used, number of estimators, number of time windows, wavelet, ... then do cross validation, compare scores, keep better configuration and repeat.
# finally, do kaggle prediction and submission with best configuration found

# fix predictions. had to invert propabilities to score above 50%. find out why!!! -> patient 1 scores are insanely bad!
# fixed. classifier can predict subject 1 very bad which was why the overall score was below .5

np.set_printoptions(linewidth=220, threshold=np.nan)

# usage: python predict.py <parent dir of h5 feature files> <submission dir> <number of time windows>
indir, outdir, nWindows = sys.argv[1], sys.argv[2], int(sys.argv[3])

# features can be added / removed in prediction through commenting
features_time = sorted([
    "complexity", 
    "energy",
    "fractaldim", 
    "kurt", 
    "linelength",
    "max", 
    "mean", 
    # "median",
    "min",
    "mobility", 
    "nonlinenergy", 
    "skew", 
    "var", 
    "zerocrossing", 
    "zerocrossingdev",
])

features_frequency = sorted([
    "boundedvariation",
    "energy",
    "entropy",
    "mean",
    "power",
    # "relativescaleenergy", # not yet implemented
    "var",
])

# these are constants over the whole project. save them externally as settings?
patient_ids = [1,2,3]
nFeaturesTime, nFeaturesFrequency, nFrequencyBands, nChannels = len(features_time), len(features_frequency), 5, 16

normalized, subsampled, oversampled, scaled = True, False, True, False
nEstimators, nJobs = 500, 20

#______________________________________________________________________________________________________________________
def buildFeatureVector(featureslist, nFiles):
    # take frequency and time features independently
    [featuresF, featuresT] = featureslist

    # reshape to allow correct stacking
    all_files_all_ffeat = []
    if nFeaturesFrequency > 0:
        split_by_features = np.reshape(featuresF, (nFeaturesFrequency, nFiles, nChannels * nWindows * nFrequencyBands))
        # for every file stack all its frequency features back to back
        for e in split_by_features:
            all_files_all_ffeat = e if len(all_files_all_ffeat) == 0 else np.hstack((all_files_all_ffeat, e))

    # reshape to allow correct stacking
    all_files_all_tfeat = []
    if nFeaturesTime > 0:
        split_by_features = np.reshape(featuresT, (nFeaturesTime, nFiles, nChannels * nWindows))
        # for every file stack all its time features back to back
        for e in split_by_features:
            all_files_all_tfeat = e if len(all_files_all_tfeat) == 0 else np.hstack((all_files_all_tfeat, e))

    # concat frequency and time features
    if nFeaturesFrequency == 0: featureV = all_files_all_tfeat
    elif nFeaturesTime == 0: featureV = all_files_all_ffeat
    else: featureV = np.hstack((all_files_all_ffeat, all_files_all_tfeat))
    return featureV

#______________________________________________________________________________________________________________________
def subsample(trainI, trainP):
    if len(trainI) > len(trainP):
        trainI = trainI[:len(trainP)]
    else:
        trainP = trainP[:len(trainP)]
    return (trainI, trainP)

#______________________________________________________________________________________________________________________
def oversample(trainI, trainP):
    newTrainI, newTrainP = trainI, trainP
    if len(trainI) > len(trainP):
        while len(newTrainI) > len(newTrainP):
            newTrainP = np.vstack((newTrainP, trainP))
    else:
        while len(newTrainP) > len(newTrainI):
            newTrainI = np.vstack((newTrainI, trainI))

    return newTrainI, newTrainP

#______________________________________________________________________________________________________________________
def computeLabels(trainI, trainP):
    labelsI = len(trainI) * [0]
    labelsP = len(trainP) * [1]
    return np.array(labelsI + labelsP)

#______________________________________________________________________________________________________________________
def computeWeights(trainI, trainP):
    weightI = len(trainP) / float(len(trainI) + len(trainP))
    weightP = len(trainI) / float(len(trainI) + len(trainP))
    weightsI = len(trainI) * [weightI]
    weightsP = len(trainP) * [weightP]
    print "weightI", weightI, "weightP", weightP
    print
    return np.array(weightsI + weightsP)

#______________________________________________________________________________________________________________________
def crossValidate(rf, trainI, trainP, labels, test_size, folds):
    segPerHour, test_size = 6, 1
    print trainI.shape, len(labels)
    print labels, len(labels) / 6 # 6 segments belong together as one hour

    labelsI = labels[:len(trainI)]
    labelsP = labels[-len(trainP):]
    print labelsI, labelsP

    X_trainI, X_testI = trainI[:-segPerHour*test_size], trainI[-segPerHour*test_size:]
    X_trainP, X_testP = trainP[:-segPerHour*test_size], trainP[-segPerHour*test_size:]
    y_trainI, y_testI = labelsI[:-segPerHour*test_size], labelsI[-segPerHour*test_size:]
    y_trainP, y_testP = labelsP[:-segPerHour*test_size], labelsP[-segPerHour*test_size:]

    print X_trainI.shape, X_testI.shape, y_trainI.shape, y_testI.shape



    exit(0)
    X_train, X_test, y_train, y_test = cross_validation.train_test_split(train, labels, test_size=test_size, random_state=0)
    # X_train, X_test, y_train, y_test = 
    rf = rf.fit(X_train, y_train)

    # TODO: unroll cross validation
    # maybe add feature importances here
    scores = cross_validation.cross_val_score(rf, X_test, y_test, cv=fold)
    print "cross validation scores", scores

    predictions = rf.predict(X_test)
    score = rf.score(X_test, y_test)
    fpr, tpr, thresholds = metrics.roc_curve(y_test, predictions)
    roc_auc = metrics.auc(fpr, tpr)
    print "auc", str(roc_auc)
    print

    f_imp = rf.feature_importances_
    return

#______________________________________________________________________________________________________________________
def sumUpFeatureImportances(f_imp):
    # corresponds to sencond last feature, here: zerocrossing time feature
    # print f_imp[-nWindows*nChannels*2:-nWindows*nChannels]

    if len(features_frequency) > 0:
        importancesF = f_imp[:nFeaturesFrequency*nWindows*nChannels*nFrequencyBands]
        splitsF = np.split(importancesF, nFeaturesFrequency)
        summedImportancesF = np.sum(splitsF, axis=1)
        stdF = np.std(splitsF, axis=1)

    # split by number of features 
    # sum over importances of every feature
    # get std of individual features
    if len(features_time) > 0:
        importancesT = f_imp[-nFeaturesTime*nWindows*nChannels:]
        splitsT = np.split(importancesT, nFeaturesTime)
        summedImportancesT = np.sum(splitsT, axis=1)
        stdT = np.std(splitsT, axis=1)

    # concat importances and std of frequency and time features
    if len(features_frequency) > 0 and len(features_time) > 0:
        return np.hstack((summedImportancesF, summedImportancesT)), np.hstack((stdF, stdT))
    elif len(features_frequency) > 0:
        return summedImportancesF, stdF
    else:
        return summedImportancesT, stdT

#______________________________________________________________________________________________________________________
def plotImportances(importances, std, patient_id):
    # sort indices by highest importance
    indices = np.argsort(importances)[::-1]
    # sort features by indices
    ff = ['f' + i for i in features_frequency]
    imp_sorted_feats = np.array(ff + features_time)[indices]

    # Plot the feature importances of the forest
    plt.figure()
    plt.title("Feature importances")
    plt.bar(range(len(importances)), importances[indices], color='r', yerr=std[indices], align="center")
    plt.xticks(range(len(importances)), imp_sorted_feats, rotation=45, ha='right')
    plt.xlim([-1, len(importances)])
    plt.savefig(os.path.join(outdir, 'importances' + str(patient_id) + '.png'), bbox_inches='tight')

#______________________________________________________________________________________________________________________
def postprocess(allPredictions):
    bigger, smaller = [], []
    # Go through all predictions to find those that are > or < 0.5 to scale them separately
    for i, prediction in enumerate(allPredictions):
        if prediction >= 0.5:
            bigger += [prediction]
        else:
            smaller += [prediction]

    # Scale > 0.5 to [0.5,1], < 0.5 to [0,0.5]
    bigger = preprocessing.minmax_scale(bigger, feature_range=(0.5, 1))
    smaller = preprocessing.minmax_scale(smaller, feature_range=(0, 0.5))

    # Go trhough all predictions again and write scaled prediction to it's position
    smallerIndice, biggerIndice = 0, 0
    for i, prediction in enumerate(allPredictions):
        if prediction >= 0.5:
            allPredictions[i] = bigger[biggerIndice]
            biggerIndice += 1
        else:
            allPredictions[i] = smaller[smallerIndice]
            smallerIndice += 1

    return allPredictions

#______________________________________________________________________________________________________________________
def main():
    allTestNames, allPredictions = [], []
    for patient_id in patient_ids:
        print "patient ", patient_id
        # read features from hdf5 file
        h5file = myio.getH5file(indir, patient_id)
        # add lists of features frequency and features time that should be used to train the classifier
        # as soon as reading only parts of hdf5 file works
        h5entries, h5counts = myio.restoreHDF5Structure(h5file, features_frequency, features_time)
        # print h5entries, h5counts
        features = myio.readFromHDF5(h5file, h5entries, normalized)
        print "h5entries", len(h5entries)

        nInterictalEntries, nPreictalEntries, nTestEntries = h5counts['interictal'], h5counts['preictal'], h5counts['test']
        trainI, trainP, test = None, None, None
        
        for case in ["interictal", "preictal", "test"]:
            nEntries = h5counts[case]
            nFiles = nEntries / (nFeaturesFrequency + nFeaturesTime)
            print case, "entries", nEntries, "files", nFiles

            if nFiles == 0:
                print "No features of interictal / preictal / test could be read from hdf5 file. Nothing to train, nothing to predict."
                exit(0)

            # get features from hdf5 reading according to current case and create the feature vector
            if case == "interictal":
                casefeats = features[:nInterictalEntries]
                casefeats = [np.array(casefeats[:nFiles*nFeaturesFrequency]), np.array(casefeats[-nFiles*nFeaturesTime:])]
                trainI = buildFeatureVector(casefeats, nFiles)
            elif case == "preictal":
                casefeats = features[nInterictalEntries:nInterictalEntries+nPreictalEntries]
                casefeats = [np.array(casefeats[:nFiles*nFeaturesFrequency]), np.array(casefeats[-nFiles*nFeaturesTime:])]
                trainP = buildFeatureVector(casefeats, nFiles)
            elif case == "test":
                casefeats = features[nInterictalEntries+nPreictalEntries:]
                casefeats = [np.array(casefeats[:nFiles*nFeaturesFrequency]), np.array(casefeats[-nFiles*nFeaturesTime:])]
                test = buildFeatureVector(casefeats, nFiles)


        # create random forest classifier
        rf = RandomForestClassifier(n_estimators=nEstimators, n_jobs=nJobs)
        # rf = AdaBoostClassifier(n_estimators=nEstimators)

        # subsample / oversample within the cross validation to not have duplicates in train and test
        if subsampled:
            trainI, trainP = subsample(trainI, trainP)
            print "subsampled trainI", trainI.shape, "trainP", trainP.shape
        elif oversampled:
            trainI, trainP = oversample(trainI, trainP)
            print "oversampled trainI", trainI.shape, "trainP", trainP.shape
        train = np.concatenate((trainI, trainP))
        print "train", train.shape

        # create labels for cross validation and training
        labels = computeLabels(trainI, trainP)
        weights = computeWeights(trainI, trainP)

        # do 5-fold cross validation on a 3/4 to 1/4 split
        crossValidate(rf, trainI, trainP, labels, 0.25, 5)
        # customCrossVal(rf, train, labels, weights, 0,25, 5)

        # do actual predictions for kaggle
        rf = rf.fit(train, labels, weights)
        # safe model to file
        myio.writeModel(rf, outdir, 'rf' + str(patient_id) + '.pkl')

        # get importances, sum em up and plot with stds
        importances = rf.feature_importances_
        summedImportances, std = sumUpFeatureImportances(importances)
        plotImportances(summedImportances, std, patient_id)

        # get predictions and take those for class 1, preictal
        predictions = rf.predict_proba(test)
        segpredictions = np.array(predictions)[:,1] # WHY IS IT 0 AND NOT 1?! IT SHOULD BE 1!! actually is 1. predictions on subj1 are very bad though

        # get the names of test files that were processed to do predictions upon.
        # get skipped files e.g. from external file and fill up with 0.5
        # these are all entries in hdf file to test
        testnames = h5entries[-nTestEntries:]
        # these are all entries to one feature
        testnames = testnames[-(nTestEntries / (nFeaturesFrequency + nFeaturesTime)):]
        # get the actual filename from the structure of hdf5 file
        testnames = [testname.split('/')[-1] + ".mat" for testname in testnames]
        allTestNames += testnames
        allPredictions  = segpredictions if len(allPredictions) == 0 else np.concatenate((allPredictions, segpredictions))

    # scale predictions to min/max
    if scaled: allPredictions = postprocess(allPredictions)

    # write predictions in kaggle submission format
    myio.createSubmission(indir, outdir, allTestNames, "", allPredictions)

#______________________________________________________________________________________________________________________
if __name__ == "__main__":
    main()
