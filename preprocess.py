# Written by Lukas Gemein
# from scipy import signal
from sklearn import preprocessing
import pandas as pd
import numpy as np
import pywt
import sys

# custom modules
from parallel import ProcessManager
import features_frequency as ff
import features_time as ft
import myio

# normalize features: min/max per segment per feature. done in myio
# TODO: swap order of dropout handling and splitting into time windows?
# TODO: fix window size instead of number of windows?
# TODO: create statistics module for zeros / skipped / nan, timing?
# TODO: add argument parser?
# TODO: move reading of data into individual tasks?

np.set_printoptions(linewidth=220, threshold=np.nan)

# usage: python preprocess.py <parent dir of data> <feature dir> [number of time windows]
indir, outdir = sys.argv[1], sys.argv[2]
if (len(sys.argv) == 4): nWindows = int(sys.argv[3])
else: nWindows = 1
filelimit, skipthresh, nproc, batchsize = [0, 3], 0.9, 20, 500
normalizeFeatures = False

# stats
allZeros, skipped, containsNaN, containsZero = [], [], [], []

# constants
patient_ids, cases, domains, waveletcode = [1,2,3], ["interictal", "preictal", "test"], ["frequency", "time"], 'db4'
# set to empty string if no safetycheck should be made
safetycsv = "train_and_test_data_labels_safe.csv"
safetycsv =''

features_time = sorted([
    "complexity", 
    "energy",
    "fractaldim", 
    "kurt", 
    "linelength",
    "max", 
    "mean", 
    # "median",
    "min",
    "mobility", 
    "nonlinenergy", 
    "skew", 
    "var", 
    "zerocrossing", 
    "zerocrossingdev",
])

features_frequency = sorted([
    "boundedvariation",
    "energy",
    "entropy",
    "mean",
    "power",
    # "relativescaleenergy", # not yet implemented
    "var",
])

#______________________________________________________________________________________________________________________
# delete parts of the data where the signal is 0 over all electrodes
def cutOutZero(eegData):
    return np.delete(eegData, np.where(~eegData.any(axis=1))[0], 0)

#______________________________________________________________________________________________________________________
# for every file transform every channel with DWT, grab the interesting frequency bands, split the individual bands into
# time windows and compute each feature of module features_frequency on every window
def computeFeatFreq(featurelist, eegData, filename):
    global containsNaN
    global containsZero

    if len(featurelist) == 0: return

    # use features_frequency module and daubechies 4 wavelet. try others?
    module = ff
    wavelet = pywt.Wavelet(waveletcode)

    tmp = []
    for chid, ch in enumerate(eegData.T):
        # compute 6 level wavelet decomposition to cover epileptic bands
        max_decomp_level = pywt.dwt_max_level(len(ch), wavelet)
        if max_decomp_level < 6:
            print "required level of wavelet transformation cannot be achieved"
            print "6th level wavelet transformation requires at least 448 samples"
            print ch, len(ch), filename
            exit(0)

        a6, d6, d5, d4, d3, d2, d = pywt.wavedec(ch, wavelet, level=6)
        freqbands = [a6, d6, d5, d4, d3]

        # TODO: check if it can be processed similarily as time features, e.g. split all freqbands at once
        freqbandfeatures = []
        # split each frequency band into nWindows
        for freqband in freqbands:
            freqband = np.array_split(freqband, nWindows, axis=0)

            for feature in featurelist:
                # for every window in the current freq band compute current feature simultaneously
                feat = getattr(module, feature)(pd.DataFrame(freqband).T)
                freqbandfeatures = feat if len(freqbandfeatures) == 0 else np.vstack((freqbandfeatures, feat))

        freqbandfeatures = freqbandfeatures.reshape(1, len(freqbands), len(features_frequency), nWindows)
        tmp = freqbandfeatures if len(tmp) == 0 else np.vstack((tmp, freqbandfeatures))

    # before: axis 0: channels, axis 1: freqbands, axis 2: features, axis 3: windows
    # after: freqfeats shape: nFeatures, nChannels, nFrequencybands, nWindows
    freqfeats = np.array(np.split(tmp, len(featurelist), axis=2))
    # 1st ch, 1st freqband, 1st window, mth window, 2nd freqband, 1st window, ... 16th ch, 6th freqband, mth window
    freqfeats = freqfeats.reshape(len(featurelist), len(eegData.T), freqfeats.size / (len(eegData.T) * len(featurelist)))

    # convert to np array to avoid problems with process manager
    freqfeats = np.array(freqfeats)
    # zero / nan check
    if np.isnan(freqfeats.any()): containsNaN += [filename]
    if np.count_nonzero(freqfeats) != freqfeats.size: containsZero += [filename]

    return freqfeats


#______________________________________________________________________________________________________________________
# for every file split it into nWindows time windows, compute each feature of module features_time on every window
def computeFeatTime(featurelist, eegData, filename):
    global containsNaN
    global containsZero

    if len(featurelist) == 0: return
    # use features_time module
    module = ft
    # eegData: 240000 x 16 / windows: nwindows x windowlength x 16
    eegwindows = np.array_split(eegData, nWindows, axis=0)
    timefeats = []
    for feature in featurelist:
        tmp = []
        # TODO: check iterations of eegwindows
        for window in eegwindows:
            tmp = np.concatenate((tmp, getattr(module, feature)(pd.DataFrame(window))))
        tmp = tmp.reshape(tmp.size / nWindows, nWindows)

        # if there is only one window, tmp is a list. cast to np array and check for nans and zeros
        tmp = np.array(tmp)
        timefeats += [tmp]

    # convert to np array to avoid problems with process manager
    timefeats = np.array(timefeats)
    # zero / nan check
    if np.isnan(timefeats.any()): containsNaN += [filename]
    if np.count_nonzero(timefeats) != timefeats.size: containsZero += [filename]

    return timefeats

#______________________________________________________________________________________________________________________
# process every file in the following way: get data from matlab struct, skip if the count of 0 is too high,
# handle data dropout and add tasks to processo manager to compute features for time and frequency domain in parallel
def caseRoutine(PM, filelist, case):
    global skipped
    global allZeros

    idstoskip = []
    for fileid, file in enumerate(filelist):
        print "Processing " + file
        # get data from matlab file. shape 240000x16
        mat = myio.readMat(indir, file)
        eegData = mat["dataStruct"].data
        # normalize raw data? no.
        # it already is 0 centered, mean was subtracted

        # don't process train file if it contains only zeros
        # DO process test files! because they need to be classified and cannot be skipped
        # skip for now, handle later
        if np.count_nonzero(eegData) == 0:
            allZeros += [file]
            idstoskip += [fileid]
            print file
            if case == "test":
                myio.writeSkipped(outdir, file)
            continue

        # Also, if there is more than thresh percent zeros, skip the file
        # don't skip testfiles. since there is information the classifier might find a better result than 0.5
        # skip since number of samples is too small to do 6 level wavelet tranformation
        if ((eegData.size - np.count_nonzero(eegData)) / float(eegData.size)) > skipthresh:
            skipped += [file]
            idstoskip += [fileid]
            print file
            if case == "test":
                myio.writeSkipped(outdir, file)
            continue 

        # handle data dropout by cutting these areas
        eegData = cutOutZero(eegData)
        print "shape after handling data dropout", eegData.shape, '\n'

        #-----------------------------------
        # write zero deleted file to hdd for visualization
        # print file
        # mat["dataStruct"].data = eegData
        # myio.writeMat(outdir, file, mat)
        # continue
        #-----------------------------------

        # add tasks to compute features to process manager so that they can be done in parallel
        PM.add_task(computeFeatFreq, args=(features_frequency, eegData, file))
        PM.add_task(computeFeatTime, args=(features_time, eegData, file))
    
    # delete files from list that have not been processed. go from back to front to ensure correctness
    for fileid in idstoskip[::-1]:
        del filelist[fileid]

    return (filelist, PM.execute(nproc=nproc))

#______________________________________________________________________________________________________________________
def subjectRoutine(patient_id, PM):
    # create hdf5 file to store features
    hdf5filepath = myio.createHDF5Structure(outdir, patient_id, cases, domains, features_time, features_frequency)

    for case in cases:
        files = myio.getFiles(indir, patient_id, case) # might add filelimit here
        # check if files are safe for training
        if safetycsv and (case == "interictal" or case == "preictal"):
            files = myio.safetyCheck(indir, safetycsv, patient_id, files)
        # split into batches of max 500 files
        files = np.array(files)
        fileslist = np.array_split(files, (len(files) / batchsize) + 1)

        # process batches of up to 500 files to prevent memory problems
        for files in fileslist:
            filelist, features = caseRoutine(PM, list(files), case)
            # TODO: could do normalization here. but since myio is iterating through the files anyway....
            myio.writeToHDF5(features, hdf5filepath, filelist, case, features_frequency, features_time, normalizeFeatures)

#______________________________________________________________________________________________________________________
def main():
    print "indir:", indir
    print "outdir:", outdir
    print "windows:", nWindows
    print "Computing"
    print "\tfreqeuncy:", features_frequency 
    print "\ttime:", features_time

    PM = ProcessManager()
    for patient_id in patient_ids:
        subjectRoutine(patient_id, PM)

    print "\nall zero files:", allZeros
    print "skipped files:", skipped
    print "files with zero:", containsZero
    print "nan files:", containsNaN

#______________________________________________________________________________________________________________________
if __name__ == "__main__":
    main()